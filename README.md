# gitalb-workflow-testing

C'est un repository qui permet d'illustrer le workflow gitlab avec un pipeline CI/CD.

## Branches

- main
- staging
- dev (develop)
- features-x

## CI/CD

### Stages

- build
- tests
- codequality
- package
- deploy

## Outils

- gitlab : versionning code 
- gitlab-ci : intégrateur de code et livraison continue
